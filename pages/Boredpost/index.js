import styles from '../../styles/Boredpost.module.css'

function Boredpost({blogposts}) {
    return (
    <>
        <div className={styles.boredblock}>
            <h1>Bored? Do this:</h1>
            <hr />
            <h2>{blogposts?.activity}</h2>
            <p>This is an API demo - using getServerSideProps to aggrigate a new activity every request.</p>
        </div>
    </>
    )
}

export default Boredpost

export async function getServerSideProps() {
    const response = await fetch('https://www.boredapi.com/api/activity')
    const data = await response.json()
    console.log(data)

    return {
        props: {
            blogposts: data,
        },
    }
}